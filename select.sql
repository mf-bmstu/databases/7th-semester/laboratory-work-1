use billing_simple;
set autocommit = 0;


-- 1. Выведите поступления денег от пользователя с email 'vasya@mail.com' . В результат включите все столбцы таблицы и не меняйте порядка их вывода.
select *
from billing
where payer_email = 'vasya@mail.com';

-- 2. Добавьте в таблицу одну запись о платеже со следующими значениями: email плательщика: 'pasha@mail.com' email получателя: 'katya@mail.com' сумма: 300.00 валюта: 'EUR' дата операции: 14.02.2016 комментарий: 'Valentines day present)'
begin;

insert into billing (payer_email, recipient_email, sum, currency, billing_date, comment)
values ('pasha@mail.com', 'katya@mail.com', 300.00, 'EUR', STR_TO_DATE('14.02.2016', '%d.%m.%Y'), 'valentines day present) ');

commit;

-- 3. измените адрес плательщика на 'igor@mail.com' для всех записей таблицы, где адрес плательщика 'alex@mail.com'.
begin;

set sql_safe_updates = 0;

update billing  
set payer_email = "igor@mail.com"
where payer_email = "alex@mail.com";

set sql_safe_updates = 1;

commit;


-- 4. удалите из таблицы записи, где адрес плательщика или адрес получателя установлен в неопределенное значение или пустую строку.
begin;

set sql_safe_updates = 0;

delete 
from billing
where (recipient_email is null) or (recipient_email = "") or (payer_email is null) or (payer_email = "");

set sql_safe_updates = 1;

commit;


-- 5. Найти отправитилей для которых количество уникальных валют использованных ими будет максимально
with currency_table as (
	select payer_email, count(currency) as unique_currency_count
	from
		(select payer_email, currency, count(currency)
		from billing
		group by payer_email, currency
		order by payer_email) as t1
	group by payer_email
	order by unique_currency_count desc, payer_email
)

select * 
from currency_table
where unique_currency_count = (select max(unique_currency_count) from currency_table) or
	  unique_currency_count = (select min(unique_currency_count) from currency_table);


-- 6. Найти отправитилей для которых количество уникальных получателей было бы максимально или минимально
with recipient_table as (
	select payer_email, count(recipient_email) as unique_recipient_count
	from 
		(select payer_email, recipient_email
		from billing
		group by payer_email, recipient_email) as t1
	group by payer_email
	order by unique_recipient_count desc, payer_email
) -- Таблица с количеством уникальных получателей для каждого отправителя

select * 
from recipient_table
where unique_recipient_count = (select max(unique_recipient_count) from recipient_table) or
	  unique_recipient_count = (select min(unique_recipient_count) from recipient_table);


-- 7. Вывести сумму полученных денег сгруппированную по валютам для всех электронных почт, что входят в 8 запрос
with recipient_table as (
	select payer_email, count(recipient_email) as unique_recipient_count
	from 
		(select payer_email, recipient_email
		from billing
		group by payer_email, recipient_email) as t1
	group by payer_email
) -- Таблица с количеством уникальных получателей для каждого отправителя


select t2.payer_email as email, currency, sum(sum) as amount_recived, t2.unique_recipient_count
from billing
right join 
	(select *
	from recipient_table
	where unique_recipient_count = (select max(unique_recipient_count) from recipient_table) or
		  unique_recipient_count = (select min(unique_recipient_count) from recipient_table)
	) as t2
on recipient_email = t2.payer_email
group by recipient_email, currency
order by recipient_email;

-- 8. Вывести сумму полученных и отправленных денег сгруппированные по валютам для всех электронных почт, что входят в 6 запрос
with full_join_t as(
	select t2.recipient_email, t3.payer_email, t2.r_currency, t3.p_currency, t2.amount_recived, t3.amount_sent
	from  (
		select recipient_email, currency as r_currency, sum(sum) as amount_recived
		from billing
		group by recipient_email, currency
	) as t2
	left join  (
		select payer_email, currency as p_currency, sum(sum) as amount_sent
		from billing
		group by payer_email, currency
	) as t3 on t3.payer_email =  recipient_email and r_currency = p_currency

	union 

	select t2.recipient_email, t3.payer_email, t2.r_currency, t3.p_currency, t2.amount_recived, t3.amount_sent
	from  (
		select recipient_email, currency as r_currency, sum(sum) as amount_recived
		from billing
		group by recipient_email, currency
	) as t2
	RIGHT JOIN  (
		select payer_email, currency as p_currency, sum(sum) as amount_sent
		from billing
		group by payer_email, currency
	) as t3 on t3.payer_email =  recipient_email and r_currency = p_currency
), -- Таблица полученных и отправленных денег для каждого email
filter_t as (
	select payer_email, count(recipient_email) as unique_recipient_count
	from 
		(select payer_email, recipient_email
		from billing
		group by payer_email, recipient_email) as t1
	group by payer_email
) -- Таблица с количеством уникальных получателей для каждого отправителя

select 
	coalesce(full_join_t.recipient_email, full_join_t.payer_email) as email,
    coalesce(r_currency, p_currency) as currency,
    coalesce(amount_recived, 0) as amount_recived,
    coalesce(amount_sent, 0) as amount_sent,
    coalesce((amount_recived - amount_sent), amount_recived, amount_sent) as delta,
    unique_recipient_count
from full_join_t 
right join filter_t on coalesce(full_join_t.recipient_email, full_join_t.payer_email) = filter_t.payer_email
where filter_t.unique_recipient_count = (select max(unique_recipient_count) from filter_t) or
	  filter_t.unique_recipient_count = (select min(unique_recipient_count) from filter_t)
order by email, currency, amount_recived, amount_sent

